package controller;

import java.util.Date;

import model.Client;
import model.Employee;
import utils.Pair;
import utils.Position;
import utils.constants.Shift;
import view.employee.EmployeeManagementInterface;

public interface BeachManagerManagement {
	
	public Client getUmbrellaInfo(Position pos);
	
	public boolean isUmbrellaAlreadyRent(Position pos);

	public int getCotNumber(Position pos);

	public double rentEquipment(int numUmbrellas, int numSunBench, Client client);

	public void giveBack(Position pos);

	public void setShift(Employee employee, Shift shift, Date date);

	public Pair<Employee, Employee> getShiftInfo(Date date);

	void setEmployeeInterface(EmployeeManagementInterface epm);

	void removeEmployee(Employee employee);
	
	void addEmployee(String name, String surname, Date birthdate);
}
