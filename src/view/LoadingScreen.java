package view;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JWindow;

/**
 * Loading screen iniziale
 * 
 * @author Samuele Medici, samuele.medici2@studio.unibo.it ( Mat. 0000718877 )
 *
 */
public class LoadingScreen extends JWindow {

	private static final long serialVersionUID = -3433519278492804933L;
	private MainView mainView;

	// Dimensione dello schermo
	private Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();

	// Dimensioni dell'immagine
	private final int width = 866;
	private final int height = 577;

	public LoadingScreen() {
		// Istanza della View principale
		this.mainView = new MainView();

		// Dimensione della schermata di caricamento
		this.setSize(new Dimension(this.width, this.height));
		
		// Aggiungo un mouse listener
		this.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				// All click del mouse sulla schermata chiudo questa schermata e apro la view
				// principale
				dispose();
				mainView.show();
			}
		});

		// Centro la schermata con le dimensioni appena settate
		this.setLocation(dimension.width / 2 - this.width / 2, dimension.height / 2 - this.height / 2);

		this.add(this.getMainPanel());
	}

	/**
	 * 
	 * @return Pannello di visualizzazione
	 */
	private JPanel getMainPanel() {
		ImageIcon imageIcon = new ImageIcon(this.getClass().getResource("/background.jpg"));

		JPanel mainPanel = new JPanel() {
			@Override
			protected void paintComponent(Graphics g) {

				super.paintComponent(g);
				g.drawImage(imageIcon.getImage(), 0, 0, null);
			}
		};

		return mainPanel;
	}

}
