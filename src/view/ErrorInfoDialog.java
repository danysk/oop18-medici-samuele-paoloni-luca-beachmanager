package view;

import java.awt.Frame;
import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Dialog di errore
 * @author Samuele Medici, samuele.medici2@studio.unibo.it ( Mat. 0000718877 )
 *
 */
public class ErrorInfoDialog extends InfoDialog {

	
	/**
	 * Generated serial version
	 */
	private static final long serialVersionUID = -3618201959380094430L;

	public ErrorInfoDialog(Frame parent, String textToShow) {
		super(parent);

		JPanel mainPanel = new JPanel(new GridBagLayout());
		// Mostra il testo di errore
		mainPanel.add(new JLabel(textToShow));
		
		this.add(mainPanel);
	}
	

}
