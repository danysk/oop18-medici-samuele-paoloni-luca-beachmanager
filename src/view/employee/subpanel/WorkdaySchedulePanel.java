package view.employee.subpanel;

import java.awt.GridBagLayout;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.stream.IntStream;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import controller.BeachManagement;
import model.Employee;
import utils.constants.Shift;

/**
 * Pannello per gestione della schedulazione degli orari
 * 
 * @author Samuele Medici, samuele.medici2@studio.unibo.it ( Mat. 0000718877 )
 *
 */
public class WorkdaySchedulePanel extends JPanel implements EmployeeListConsumer {

	
	private static final long serialVersionUID = -3995710509958222251L;

	// Lista dipendenti
	private Employee[] employeeList;

	// ComboBox per la scelta del cambio turno
	private JComboBox<String> employeeComboBox = new JComboBox<String>();
	private JComboBox<Integer> monthDayComboBox = new JComboBox<Integer>();
	private JComboBox<Integer> weekDayComboBox = new JComboBox<Integer>();
	private JComboBox<String> shiftComboBox = new JComboBox<String>();

	// Controller
	private BeachManagement management;

	private final int daysOfMonths[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	
	// Valori correnti
	private final int currentYear = Calendar.getInstance().get(Calendar.YEAR);
	private final int currentMonth = Calendar.getInstance().get(Calendar.MONTH);
	private final int currentDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
	
	public WorkdaySchedulePanel(Employee[] employeeList, BeachManagement management) {
		super();

		// GridBagLayout per centrare i componenti
		this.setLayout(new GridBagLayout());

		this.management = management;

		// Aggiungo tutti i possibili turni
		Arrays.stream(Shift.values()).forEach(shift -> {
			this.shiftComboBox.addItem(shift.getValue());
		});

		// Ottengo i giorni della settimana
		// - 1 all'indice perchè l'array parte da 0
		// + 1 perchè il finale del range è esclusivo
		IntStream.range(1, this.daysOfMonths[this.currentMonth - 1] + 1).forEach(weekday -> {
			this.weekDayComboBox.addItem(weekday);
		});;
		
		// Aggiungo i mesi ( + 1 perché è esclusivo )
		IntStream.range(1, 12 + 1).forEach(month -> {
			this.monthDayComboBox.addItem(month);
		});
		
		// SETTO I VALORI INIZIALI
		this.weekDayComboBox.setSelectedIndex(this.currentDay);
		this.monthDayComboBox.setSelectedIndex(this.currentMonth);
		
		this.monthDayComboBox.addActionListener(e -> {
			this.weekDayComboBox.removeAllItems();
			int month = (Integer) this.monthDayComboBox.getSelectedItem();
			IntStream.range(1, this.daysOfMonths[month - 1] + 1).forEach(weekday -> {
				this.weekDayComboBox.addItem(weekday);
			});;
		});

		JButton changeShift = new JButton("Assegna turno");
		changeShift.addActionListener(e -> {
			// Ottieni i valori dalle comboBox
			Employee emp = this.employeeList[this.employeeComboBox.getSelectedIndex()];
			int weekday = (Integer) this.weekDayComboBox.getSelectedItem();
			int month = (Integer) this.monthDayComboBox.getSelectedItem();
			Shift shift = this.shiftComboBox.getSelectedIndex() == 0 ? Shift.MORNING : Shift.AFTERNOON;
			
			Date shiftDate = new GregorianCalendar(this.currentYear, month, weekday).getTime();
			// Azione del controller
			this.management.setShift(emp, shift, shiftDate);
		});

		this.add(this.employeeComboBox);
		this.add(this.weekDayComboBox);
		this.add(monthDayComboBox);
		this.add(shiftComboBox);

		this.updateEmployeeList(employeeList);

		this.add(changeShift);
	}

	/**
	 * Aggiorna la View
	 */
	private void updateView() {
		this.employeeComboBox.removeAllItems();

		// Utilizzo gli stream che sono più dichiarativi
		Arrays.stream(this.employeeList).forEach(employee -> {
			this.employeeComboBox.addItem(employee.getFullName());
		});

		this.resetInputs();
	}

	@Override
	public void updateEmployeeList(Employee[] employeeList) {
		this.employeeList = employeeList;
		this.updateView();
	}

	/**
	 * Reset ai valori di default
	 */
	private void resetInputs() {
		this.employeeComboBox.setSelectedIndex(0);
		this.weekDayComboBox.setSelectedIndex(0);
		this.monthDayComboBox.setSelectedIndex(0);
		this.shiftComboBox.setSelectedIndex(0);
	}

}
