package view.umbrella;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import controller.BeachManagerManagement;
import model.Client;
import utils.Position;
import view.InfoDialog;

/**
 * Dialog che mostra le informazioni legate all'ombrello
 * 
 * @author Samuele Medici, samuele.medici2@studio.unibo.it ( Mat. 0000718877 )
 *
 */

public class UmbrellaInfoDialog extends InfoDialog {


	private static final long serialVersionUID = -7657943773272051506L;

	// Label clienti
	private JLabel clientLabel;
	private JLabel sunbenchLabel;
	
	private JPanel clientPanel;

	/**
	 * 
	 * @param frame Frame padre
	 * @param position Posizione dell'ombrellone
	 * @param booking Controller per ottenere informazioni
	 */
	public UmbrellaInfoDialog(JFrame frame, Position position, BeachManagerManagement booking) {
		super(frame);
		
		// Pannelli contenitori
		JPanel parentPanel = new JPanel(new GridBagLayout());
		JPanel mainPanel = new JPanel();
		this.clientPanel = new JPanel();
		
		// BoxLayout permette di avere elementi in verticale
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		this.clientPanel.setLayout(new BoxLayout(this.clientPanel, BoxLayout.Y_AXIS));
		
		// Label con le informazioni base dell'ombrellone
		mainPanel.add(new JLabel("Fila: " + (position.getRow() + 1)));
		mainPanel.add(new JLabel("Numero della fila: " + (position.getNumber() + 1)));
			
		// Se l'ombrello è stato prenotato fornisco le informazioni del cliente
		if (booking.isUmbrellaAlreadyRent(position)) {
			// Label clienti
			Client client = booking.getUmbrellaInfo(position);
			this.clientLabel = new JLabel("Cliente: " + client.getName() + " " + client.getLastname());
			this.sunbenchLabel = new JLabel("Numero stendini: " + booking.getCotNumber(position));
			
			this.clientPanel.add(this.clientLabel);
			this.clientPanel.add(this.sunbenchLabel);
			
			// Bottone per restituire oggetti
			JButton giveBackButton = new JButton("Restituisci");
			giveBackButton.addActionListener(e -> {
				booking.giveBack(position);
				this.clientPanel.setVisible(false);
			});

			this.clientPanel.add(giveBackButton);
			
			mainPanel.add(this.clientPanel);
			
		}
		// Aggiungo con Constraints per centrare
		parentPanel.add(mainPanel, new GridBagConstraints());
		this.add(parentPanel);
	}
	
	
	
}
