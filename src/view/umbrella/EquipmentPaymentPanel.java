package view.umbrella;

import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import controller.BeachManagerManagement;
import model.ClientImpl;
import view.ErrorInfoDialog;

/**
 * Classe che permette l'acquisto di stendini e ombrelloni da parte per il
 * cliente
 * 
 * @author Samuele Medici, samuele.medici2@studio.unibo.it (Mat. 0000718877 )
 * 
 */
public class EquipmentPaymentPanel extends JPanel {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 1777256332046557165L;

	// Controller per l'affitto dell'equipaggiamento da spiaggia
	private BeachManagerManagement equipmentBooking;

	// Costo finale
	private double totalAmount;
	private JLabel amountText = new JLabel("");

	// Form inserimento dati cliente
	private JTextField clientName = new JTextField(30);
	private JTextField clientSurname = new JTextField(30);

	private JTextField clientBirthdate = new JTextField(2);
	private JTextField clientBirthmonth = new JTextField(2);
	private JTextField clientBirthyear = new JTextField(4);

	// Bottone per affitare gli stendini e/o stendini
	private JButton bookingButton;
	// Flag per l'abilitazione del bottone
	private boolean enableBookingButton = false;

	// Valori per gli spinner
	private final int minimumValue = 0;
	private final int maximumValue = 4;
	private final int step = 1;

	// Pannello per il totale dei costi
	private JPanel totalPanel;

	// Spinner model
	private SpinnerNumberModel umbrellaModel = new SpinnerNumberModel(this.minimumValue, this.minimumValue,
			this.maximumValue, this.step);
	private SpinnerNumberModel sunbenchModel = new SpinnerNumberModel(this.minimumValue, this.minimumValue,
			this.maximumValue, this.step);

	// Spinner
	private JSpinner umbrellaSpinner = new JSpinner(this.umbrellaModel);
	private JSpinner sunbenchSpinner = new JSpinner(this.sunbenchModel);

	public EquipmentPaymentPanel(BeachManagerManagement booking) {
		// Layout in una colonna
		super();
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		this.equipmentBooking = booking;

		this.bookingButton = new JButton("Affitta");

		// Listener per il calcolo dei costi
		this.bookingButton.addActionListener(e -> {
			this.calculateCost();
		});

		// Disabilito il bottone
		this.bookingButton.setEnabled(this.enableBookingButton);

		// Document Listener, ad ogni evento controllo la possibilità di acquistare
		final DocumentListener documentListener = new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent arg0) {
				checkEnablingStatus();
			}

			@Override
			public void insertUpdate(DocumentEvent arg0) {
				checkEnablingStatus();
			}

			@Override
			public void changedUpdate(DocumentEvent arg0) {
				checkEnablingStatus();
			}

		};

		// Aggiungo listener per la modifica del testo
		this.clientName.getDocument().addDocumentListener(documentListener);
		this.clientSurname.getDocument().addDocumentListener(documentListener);
		this.clientBirthdate.getDocument().addDocumentListener(documentListener);
		this.clientBirthmonth.getDocument().addDocumentListener(documentListener);
		this.clientBirthyear.getDocument().addDocumentListener(documentListener);

		this.umbrellaSpinner.addChangeListener(e -> {
			int numUmbrellaSelected = (Integer) this.umbrellaSpinner.getValue();
			// Posso prendere un numero di standini pari sempre a Numero di Ombrelli * 4
			this.sunbenchModel = new SpinnerNumberModel(this.minimumValue, this.minimumValue,
					this.maximumValue * numUmbrellaSelected, this.step);
			
			this.sunbenchSpinner.setModel(this.sunbenchModel);
			this.checkEnablingStatus();
		});
		this.sunbenchSpinner.addChangeListener(e -> {
			this.checkEnablingStatus();
		});

		// Pannelli di input
		JPanel umbrellaPanel = new JPanel();
		umbrellaPanel.add(new JLabel("Numero di ombrelli: "));
		umbrellaPanel.add(this.umbrellaSpinner);

		JPanel sunbenchPanel = new JPanel();
		sunbenchPanel.add(new JLabel("Numero di stendini: "));
		sunbenchPanel.add(this.sunbenchSpinner);

		JPanel clientPanel = this.buildClientPanel();

		this.totalPanel = new JPanel();
		this.totalPanel.add(new JLabel("Totale: "));
		this.totalPanel.add(this.amountText);

		JButton okButton = new JButton("Ok");
		okButton.addActionListener(e -> {
			this.clearAllInputs();
			this.totalPanel.setVisible(false);
		});
		this.totalPanel.add(okButton);

		this.totalPanel.setVisible(false);

		this.add(umbrellaPanel);
		this.add(sunbenchPanel);
		this.add(clientPanel);

		this.add(this.bookingButton);

		this.add(totalPanel);

	}

	/**
	 * Metodo per pulire tutti gli input
	 */
	private void clearAllInputs() {
		this.clientName.setText("");
		this.clientSurname.setText("");
		this.clientBirthdate.setText("");
		this.clientBirthmonth.setText("");
		this.clientBirthyear.setText("");

		this.umbrellaSpinner.setValue(0);
		this.sunbenchSpinner.setValue(0);

	}

	/**
	 * Metodo che controlla stato degli input per abilitare il bottone di calcolo
	 */
	private void checkEnablingStatus() {
		int minYear = 1939;
		int maxYear = 2001;
		// Try-catch per controllare qualcosa che non è un numero nelle celle di data di nascita
		try {
			int birthday = 0, birthmonth = 0, birthyear = 0;

			int umbrellaValue = (Integer) this.umbrellaSpinner.getValue();
			// int cotValue = (Integer) this.cotSpinner.getValue();
			String clientValue = this.clientName.getText();
			String surnameValue = this.clientSurname.getText();

			if (!this.clientBirthdate.getText().isEmpty()) {
				birthday = Integer.parseInt(this.clientBirthdate.getText());
			}
			if (!this.clientBirthmonth.getText().isEmpty()) {
				birthmonth = Integer.parseInt(this.clientBirthmonth.getText());
			}
			if (!this.clientBirthyear.getText().isEmpty()) {
				birthyear = Integer.parseInt(this.clientBirthyear.getText());
			}

			// Il bottone è abilitato se il nome cliente non è vuoto e se ha almeno un
			// ombrello o uno stendino
			this.enableBookingButton = (!clientValue.isEmpty() && !surnameValue.isEmpty() && umbrellaValue != 0 && birthday > 0 && birthday < 32
					&& birthmonth > 0 && birthmonth < 13 && birthyear > minYear && birthyear < maxYear);
			this.bookingButton.setEnabled(this.enableBookingButton);

		} catch (NumberFormatException e) {
			// Istanza una dialog di errore
			new ErrorInfoDialog((JFrame) SwingUtilities.getWindowAncestor(this), "E' stata inserita una stringa in input numerale!")
					.setVisibility(true);
			// In un input della data di nascita vi è un carattere e quindi le pulisco tutte
			this.clientBirthdate.setText("");
			this.clientBirthmonth.setText("");
			this.clientBirthyear.setText("");
		}

	}

	/**
	 * Metodo per calcolare costi per affittare
	 */
	private void calculateCost() {
		// Ottengo tutti i dati dagli input e li mando al controller
		int numUmbrellas = (Integer) this.umbrellaSpinner.getValue();
		int numSunbenches = (Integer) this.sunbenchSpinner.getValue();

		String clientName = this.clientName.getText();
		String clientLastName = this.clientSurname.getText();

		int birthDate = Integer.parseInt(this.clientBirthdate.getText());
		int birthMonth = Integer.parseInt(this.clientBirthmonth.getText());
		int birthYear = Integer.parseInt(this.clientBirthyear.getText());

		Date birthdate = new GregorianCalendar(birthYear, birthMonth, birthDate).getTime();

		this.totalAmount = this.equipmentBooking.rentEquipment(numUmbrellas, numSunbenches,
				new ClientImpl(clientName, clientLastName, birthdate, new Date()));

		// Se è diverso da 0 vuol dire che è andato bene il procedimento
		if (this.totalAmount != 0.0) {
			this.totalPanel.setVisible(true);
			this.amountText.setText(Double.toString(this.totalAmount));	
		} else {
			// Se è uguale a 0 vuol dire che non ci sono più ombrelloni disponibili
			new ErrorInfoDialog((JFrame) SwingUtilities.getWindowAncestor(this), "Non ci sono più ombrelloni disponibili!")
			.setVisibility(true);
			
			this.clearAllInputs();
		}
		
	}

	/**
	 * Costruisce pannello per inserimento dati cliente
	 * 
	 * @return Pannello cliente
	 */
	private JPanel buildClientPanel() {

		JPanel clientPanel = new JPanel();

		clientPanel.add(new JLabel("Nome cliente: "));
		clientPanel.add(this.clientName);

		clientPanel.add(new JLabel("Cognome cliente: "));
		clientPanel.add(this.clientSurname);

		clientPanel.add(new JLabel("Giorno di nascita: "));
		clientPanel.add(this.clientBirthdate);

		clientPanel.add(new JLabel("Mese: "));
		clientPanel.add(this.clientBirthmonth);

		clientPanel.add(new JLabel("Anno: "));
		clientPanel.add(this.clientBirthyear);

		return clientPanel;
	}

}
